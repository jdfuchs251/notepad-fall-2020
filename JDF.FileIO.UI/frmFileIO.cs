﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JDF.FileIO.UI
{
    public partial class frmFileIO : Form
    {
        // Form-level variables for handling file manipulation
        bool isTextEdited = false;
        string currentFile = string.Empty;

        // Form-level variables for handling error display
        const int ALERT_DISPLAY_SECONDS = 3; // Number of seconds a status bar alert should display
        int alertSecondCount;
        string resetStatus; // Container to hold previous value in lblStatus.Text (usually a file name)

        public frmFileIO()
        {
            InitializeComponent();
            lblDateTime.Text = DateTime.Now.ToString(); // Set time on form load so that we don't wait until the next second.
        }

        private void timerDate_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString(); // Update date and time every second
        }

        private void txtText_TextChanged(object sender, EventArgs e)
        {
            isTextEdited = true;
        }

        private void mnuFileNew_Click(object sender, EventArgs e)
        {
            if (ValidateSaveStatus())
            {
                // Reset textbox and form variables
                txtText.Text = string.Empty;
                isTextEdited = false;
                currentFile = string.Empty;
                resetStatus = string.Empty;
                lblStatus.Text = string.Empty;
            }
        }

        private void mnuFileOpen_Click(object sender, EventArgs e)
        {
            if (ValidateSaveStatus()) OpenFile();
        }

        private void mnuFileSave_Click(object sender, EventArgs e)
        {
            SelectSaveOperation();
        }

        private void mnuFileSaveAs_Click(object sender, EventArgs e)
        {
            SaveFileAs();
        }

        /**********************************************************************************
        /* Name: OpenFile()
        /* Description: Prompts user to select a file to open. Opens the selected file.
        /* Returns: void
        /*********************************************************************************/
        private void OpenFile()
        {
            try
            {
                // Set up OpenFileDialog
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "Select a File to Open";
                openFileDialog.Filter = "Text|*.txt|All Files|*";
                openFileDialog.DefaultExt = "txt";

                // Open the OpenFileDialog. If ok, load the file in the text editor.
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamReader streamReader = File.OpenText(openFileDialog.FileName);
                    txtText.Text = streamReader.ReadToEnd();
                    streamReader.Close();
                    streamReader = null;

                    // Reset form variables
                    isTextEdited = false;
                    currentFile = openFileDialog.FileName;
                    lblStatus.Text = currentFile;
                }
                else throw new Exception("Open file cancelled.");
            }
            catch (Exception ex)
            {
                DisplayAlert(ex.Message, Color.Red);
            }
        }

        /**********************************************************************************
        /* Name: SelectSaveOperation()
        /* Description: Determine whether to perform a Save or a Save As operation. If there
        /*   is an existing file open, saves to that file. Otherwise, saves as a new file.
        /* Returns: If file saved successfully, returns true. Otherwise, returns false.
        /*********************************************************************************/
        private bool SelectSaveOperation()
        {
            if (File.Exists(currentFile)) return SaveFile(currentFile);
            else
            {
                // If we have a path in currentFile, but that file does not exist. This
                // should only happen if the file is moved or deleted while the application
                // is running. Warn the user with a messagebox and do a full Save As.
                if (currentFile != string.Empty)
                {
                    MessageBox.Show(currentFile + " no longer exists. Please save as a new file.");
                    currentFile = string.Empty; // Clear currentFile since it no longer exists
                    lblStatus.Text = currentFile;
                }
                return SaveFileAs();
            }
        }

        /**********************************************************************************
        /* Name: SaveFile()
        /* Description: Writes textbox data to the specified file.
        /* Parameters:
        /*   fileName (byVal, required) - Full file name where file is to be saved
        /* Returns: If file saved successfully, returns true. Otherwise, returns false.
        /*********************************************************************************/
        private bool SaveFile(string fileName)
        {
            bool success = false;
            try
            {
                StreamWriter streamWriter = File.CreateText(fileName);
                streamWriter.WriteLine(txtText.Text);
                streamWriter.Close();
                streamWriter = null;
                isTextEdited = false;
                success = true;
                DisplayAlert("File saved.", Color.Blue);
            }
            catch (Exception ex)
            {
                DisplayAlert(ex.Message, Color.Red);
            }
            return success;
        }

        /**********************************************************************************
        /* Name: SaveFileAs()
        /* Description: Prompts the user for a path, and saves a new file.
        /* Returns: If file saved successfully, returns true. Otherwise, returns false.
        /*********************************************************************************/
        private bool SaveFileAs()
        {
            bool success = false;
            try
            {
                // Set up SaveFileDialog
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Title = "Save New File";
                saveFileDialog.Filter = "Text|*.txt|All Files|*";
                saveFileDialog.DefaultExt = "txt";

                // Open SaveFileDialog. If ok, write the file.
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    success = SaveFile(saveFileDialog.FileName);
                    if (success)
                    {
                        currentFile = saveFileDialog.FileName;
                        resetStatus = currentFile; // Do so status label displays file name when alert timer runs out
                    }
                }
                else throw new Exception("File was not saved.");
            }
            catch (Exception ex)
            {
                DisplayAlert(ex.Message, Color.Red);
            }
            return success;
        }

        /**********************************************************************************
        /* Name: ValidateSaveStatus()
        /* Description: This function is called whenever a user tries to create a new or
        /*   open an existing file. If text has been changed, prompts the user to see
        /*   whether the text should be saved before continuing. If so, performs the save
        /*   operation.
        /* Returns: If it's ok to continue with the new or open file operation, returns
        /*   true. Otherwise, returns false.
        /*********************************************************************************/
        private bool ValidateSaveStatus()
        {
            if (isTextEdited)
            {
                DialogResult dialogResult = MessageBox.Show("File has not been saved since last edit.\n" +
                                                            "Would you like to save it?", "Warning",
                                                            MessageBoxButtons.YesNoCancel);
                if (dialogResult == DialogResult.Yes)
                {
                    return SelectSaveOperation(); // Return whether or not the save was successful
                }
                else if (dialogResult == DialogResult.No)
                {
                    DisplayAlert("Changes discarded.", Color.Red);
                    return true;
                }
                else
                {
                    DisplayAlert("Cancelled.", Color.Red);
                    return false;
                }
            }
            else return true; // Text has not been edited. Ok to continue.
        }

        /**********************************************************************************
        /* Name: DisplayAlert()
        /* Description: Generic function to display an alert in the status bar.
        /* Parameters:
        /*   alertMsg (byVal, required) - Text to display in the status bar
        /*   color (byVal, required) - Color in which the alert will display
        /* Returns: void
        /*********************************************************************************/
        private void DisplayAlert(string alertMsg, Color color)
        {
            resetStatus = lblStatus.Text; // Cache current label text
            lblStatus.Text = alertMsg; // Display the alert message
            lblStatus.ForeColor = color; // Color the alert message
            alertSecondCount = 0; // Reset how long timer has been running
            timerAlert.Enabled = true; // Start the timer
        }

        private void timerAlert_Tick(object sender, EventArgs e)
        {
            alertSecondCount++; // Increment how many seconds alert has been on
            if (alertSecondCount == ALERT_DISPLAY_SECONDS)
            {
                lblStatus.Text = resetStatus; // Set label back to cached text
                lblStatus.ForeColor = Color.Black; // Return color to black
                timerAlert.Enabled = false; // Stop the timer
            }
        }
    }
}
